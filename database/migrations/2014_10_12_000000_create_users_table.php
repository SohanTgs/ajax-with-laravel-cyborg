<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->id();
            $table->string('first_name')->default('None');
            $table->string('last_name')->default('None');
            $table->integer('phone')->nullable();
            $table->string('email',255)->unique();
            $table->string('address',255)->default('None');
            $table->string("admin_type",10)->default('None');
            $table->string("admin_status",10)->default('None');
            $table->string("image",255)->default('None');
            $table->text("remark")->default('None');
            $table->timestamp('email_verified_at')->nullable();
            $table->string('password');
            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
