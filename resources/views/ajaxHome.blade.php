 <!DOCTYPE html>
<html>
<head>
<title>Ajax Example</title>
<meta name="author" content="Mohammad Sohan <php.sohan@gmail.com>">
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
</head>
<body onload="getTable();">
<!-- Modal -->
<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Modal title</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true" onclick="closeId()">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <div align='center' id="updateId"></div>
       <form id="form3" action="#" method="POST">
         <input type="hidden" name="id" id="id2">  
         <input type="text" name="test" id="test2">    
      </div>
      <div class="modal-footer">
        <button type="button" onclick="closeId()" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="button" id="btn3" class="btn btn-primary">Submit</button>
        </form>
      </div>
    </div>
  </div>
</div>
 <form action="" name="form" method="POST" id="form">
	@csrf
	<input type="text" name="test" id="test">
<!-- 	<input type="text" name="test" id="test2"> -->
	<input type="submit" name="btn" id="btn" value="Submit">
</form>

<br/>

<?php 
// <table width="50%" border="1">
// <thead>
//   <tr>
//     <th>Company</th>
//     <th>Contact</th>
//      <td>Action</td>
//   </tr>
// </thead>
// <tbody>
// 
// </tbody>
// </table>
?>

<script>
  $(document).ready(function(){
  $("#myInput").on("keyup", function() {
    var value = $(this).val().toLowerCase();
    $("#tbody tr").filter(function() {
      $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
    });
  });
});
</script>

<input id="myInput" type="text" placeholder="Search..">

<table width="50%" border="1">
  <thead>
    <tr>
      <th>Serial</th>
      <th>Name</th>
      <th>Action</th>
    </tr>
  </thead>
 <form id="form2">
  <tbody id="tbody">

  </tbody>
</form>
</table>
<!-- {{ $errors->has('test') ? $errors->first('test'):'' }} -->
<div id="result"></div>
<script>
// $.ajaxSetup({
//     headers: {
//         'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
//     }
// });

$(document).ready(function(){

  // alert(22);
    $("#form").submit(function(e){

        e.preventDefault();

        var data = $(this).serialize();
        // console.log(data);
        var url = '{{ url('ajaxPost') }}';

        $.ajax({
           url:url,
           method:'POST',
           data:data,
           success:function(response){
           	 if(response.success){
           	 	document.getElementById("result").innerHTML = response.message; 
           	  document.getElementById("test").value = '';
              getTable()
             	// $('#test').html('');
              // location.reload();
           	 }
           },
           error:function(error){
              console.log(error)
           }
        });
	});

}); 
</script>

<script>
// $.ajaxSetup({
//     headers: {
//         'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
//     }
// });
function del(id){
	// console.log(id); 
// var url = "{{ url('ajaxDel') }}";
	   $.ajax({
           url:'ajaxDel/'+id,
           method:'get',
           data:{
            id:id,
           },
           success:function(response){
           if(response == 'ok'){
            document.getElementById("result").innerHTML = "Deleted successflly";
            getTable();
            // $('#'+id).hide(); 
           }
           },
           error:function(error){
              console.log(error);
           }
        });
}
        
</script>

<script>
  function getTable(){
      $.ajax({
           url:'getTable',
           method:'get',
           // data:data,
           success:function(response){
            // console.log(response);
             document.getElementById("tbody").innerHTML = response;
            
           },
           error:function(error){
              console.log(error)
           }
        });

  }
</script>

<script>
  function edit(id){
    // console.log(id);
     $.ajax({
           url:'editId/'+id,
           method:'get',
           // data:data,
           success:function(response){
            // console.log(response.test);
             document.getElementById("test2").value = response.test;
             document.getElementById("id2").value = response.id;
            
           },
           error:function(error){
              console.log(error)
           }
        });

  }

 $('#btn3').click(function(){
    $('#form3').submit(function(e){e.preventDefault()})
    // alert(21);
    var test = $('#test2').val();
     var id2 = $('#id2').val();
    // console.log(id2);
        $.ajax({
           url:'ajaxUpdate',
           method:'POST',
           data:{
            'test':test,
            'id2':id2,
            '_token': '{{ csrf_token() }}',
           },
           success:function(response){
            if(response.success){
              document.getElementById('updateId').innerHTML = response.message;
              getTable();
            }
           },
           error:function(error){
              console.log(error);
           }
        });

      function closeId(){
        alert(21);
      }

  });
      function closeId(){
          document.getElementById('updateId').innerHTML = '';
      }

</script>
</body>
</html> 