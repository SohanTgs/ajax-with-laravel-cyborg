@extends('admin.master.master')

@section('title')
	Admin | | Add New Admin
@endsection('title')

@section('body')
<body class="page-body" cz-shortcut-listen="true">
        <!--<body class="page-body  skin-white loaded">-->
        <div class="page-container  horizontal-menu "><!-- add class "sidebar-collapsed" to close sidebar by default, "chat-visible" to make chat appear always -->
    <header class="navbar navbar-fixed-top  hidden-print"><!-- set fixed position by adding class "navbar-fixed-top" -->
        <div class="navbar-inner">
            <!-- logo -->
            <div style="height:28px !important;" class="navbar-brand">
                <a href="{{ url('dashboard') }}">
                    <img src="{{ asset('/') }}/black.png" alt="" class="img-thumbnail" width="120">
                </a>
            </div>
            <!-- main menu -->
            <ul class="navbar-nav">
                                    <li>
                        <a href="">
                            <i class="entypo-gauge"></i>
                            <span class="title">Dashboard</span>
                        </a>
                                            </li>
                                    <li>
                        <a href="">
                            <i class="glyphicon glyphicon-user"></i>
                            <span class="title">Admin</span>
                        </a>
                                                    <ul>
                                                                        <li>
                                            <a href="{{ route('admin_list') }}">
                                                <span class="title">Admin List</span>
                                            </a>
                                        </li>
                                                                                <li>
                                            <a href="{{ route('add_new_admin') }}">
                                                <span class="title">Add New Admin</span>
                                            </a>
                                        </li>
                                                                                <li>
                                            <a href="admin_login_history">
                                                <span class="title">Login History</span>
                                            </a>
                                        </li>
                                                                                <li>
                                            <a href="add_access_control">
                                                <span class="title">Access Control</span>
                                            </a>
                                        </li>
                                                                    </ul>
                                            </li>
                                    <li>
                 <!--       <a href="">
                            <i class="glyphicon glyphicon-user"></i>
                            <span class="title">Customer</span>
                        </a>
                                                    <ul>
                                                                        <li>
                                            <a href="add_new_customer">
                                                <span class="title">Add New Customer</span>
                                            </a>
                                        </li>
                                                                                <li>
                                            <a href="customer_list">
                                                <span class="title">Customer List</span>
                                            </a>
                                        </li>
                                                                    </ul>
                                            </li>
                                    <li>
                        <a href="">
                            <i class="glyphicon glyphicon-th-large"></i>
                            <span class="title">Design</span>
                        </a>
                                                    <ul>
                                                                        <li>
                                            <a href="add_new_design">
                                                <span class="title">Add New Design</span>
                                            </a>
                                        </li>
                                                                                <li>
                                            <a href="design_list">
                                                <span class="title">Design List</span>
                                            </a>
                                        </li>
                                                                    </ul>
                                            </li>
                                    <li>
                        <a href="">
                            <i class="glyphicon glyphicon-th-list"></i>
                            <span class="title">Article </span>
                        </a>
                                                    <ul>
                                                                        <li>
                                            <a href="add_new_artical">
                                                <span class="title">Add New Article </span>
                                            </a>
                                        </li>
                                                                                <li>
                                            <a href="artical_list">
                                                <span class="title">Article List</span>
                                            </a>
                                        </li>
                                                                    </ul>
                                            </li>
                                    <li>
                        <a href="">
                            <i class="entypo-gauge"></i>
                            <span class="title">Report</span>
                        </a>
                                                    <ul>
                                                                        <li>
                                            <a href="dispart_plan_form">
                                                <span class="title">Dis-part Plan</span>
                                            </a>
                                        </li>
                                                                                <li>
                                            <a href="accessorize">
                                                <span class="title">Accessorize</span>
                                            </a>
                                        </li>
                                                                    </ul>
                                            </li>
                                <li>
                    <a href="#">
                        <i class="glyphicon glyphicon-cog"></i>
                        <span class="title">Program</span>
                    </a>
                    <ul>
                                                    <li>
                                <a href="#">
                                    <i class="glyphicon glyphicon-book"></i>
                                    <span class="title">Order Program</span>
                                </a>
                                                                    <ul>
                                                                                        <li>
                                                    <a href="add_new_order">
                                                        <span class="title">Add Order Program</span>
                                                    </a>
                                                </li>
                                                                                                <li>
                                                    <a href="order_list">
                                                        <span class="title">Order List</span>
                                                    </a>
                                                </li>
                                                                                    </ul>
                                                            </li>
                                                    <li>
                                <a href="#">
                                    <i class="glyphicon glyphicon-book"></i>
                                    <span class="title">Stock Program</span>
                                </a>
                                                                    <ul>
                                                                                        <li>
                                                    <a href="add_new_stock">
                                                        <span class="title">Add Stock Program</span>
                                                    </a>
                                                </li>
                                                                                                <li>
                                                    <a href="stock_list">
                                                        <span class="title">StockList</span>
                                                    </a>
                                                </li>
                                                                                    </ul>
                                                            </li>
                                                    <li>
                                <a href="#">
                                    <i class="glyphicon glyphicon-book"></i>
                                    <span class="title">LDP Program</span>
                                </a>
                                                                    <ul>
                                                                                        <li>
                                                    <a href="add_new_ldp">
                                                        <span class="title">Add LDP Program</span>
                                                    </a>
                                                </li>
                                                                                                <li>
                                                    <a href="ldp_list">
                                                        <span class="title">LDP List</span>
                                                    </a>
                                                </li>
                                                                                    </ul>
                                                            </li>
                                                    <li>
                                <a href="#">
                                    <i class="glyphicon glyphicon-book"></i>
                                    <span class="title">Sample Program</span>
                                </a>
                                                                    <ul>
                                                                                        <li>
                                                    <a href="add_new_sample">
                                                        <span class="title">Add Sample Program</span>
                                                    </a>
                                                </li>
                                                                                                <li>
                                                    <a href="sample_list">
                                                        <span class="title">Sample List</span>
                                                    </a>
                                                </li>
                                                                                    </ul>
                                                            </li>
                                            </ul>
                </li>

                <li>
                    <a href="#">
                        <i class="glyphicon glyphicon-cog"></i>
                        <span class="title">Setup</span>
                    </a>
                    <ul>
                                                    <li>
                                <a href="#">
                                    <i class="glyphicon glyphicon-wrench"></i>
                                    <span class="title"> Product Category</span>
                                </a>
                                                                    <ul>
                                                                                        <li>
                                                    <a href="product_category_list">
                                                        <span class="title">Category List</span>
                                                    </a>
                                                </li>
                                                                                    </ul>
                                                            </li>
                                                    <li>
                                <a href="#">
                                    <i class="glyphicon glyphicon-wrench"></i>
                                    <span class="title"> Color</span>
                                </a>
                                                                    <ul>
                                                                                        <li>
                                                    <a href="add_new_color">
                                                        <span class="title">Add New Color</span>
                                                    </a>
                                                </li>
                                                                                                <li>
                                                    <a href="color_list">
                                                        <span class="title">Color List</span>
                                                    </a>
                                                </li>
                                                                                    </ul>
                                                            </li>
                                                    <li>
                                <a href="#">
                                    <i class="glyphicon glyphicon-wrench"></i>
                                    <span class="title">Size</span>
                                </a>
                                                                    <ul>
                                                                                        <li>
                                                    <a href="add_new_size">
                                                        <span class="title">Add New Size</span>
                                                    </a>
                                                </li>
                                                                                                <li>
                                                    <a href="size_list">
                                                        <span class="title">Size List</span>
                                                    </a>
                                                </li>
                                                                                    </ul>
                                                            </li>
                                                    <li>
                                <a href="#">
                                    <i class="glyphicon glyphicon-align-center"></i>
                                    <span class="title">Product </span>
                                </a>
                                                                    <ul>
                                                                                        <li>
                                                    <a href="product_list">
                                                        <span class="title">Product List</span>
                                                    </a>
                                                </li>
                                                                                                <li>
                                                    <a href="add_new_product">
                                                        <span class="title">Add New Product</span>
                                                    </a>
                                                </li>
                                                                                    </ul>
                                                            </li>
                                                    <li>
                                <a href="#">
                                    <i class="glyphicon glyphicon-book"></i>
                                    <span class="title">Setup Category</span>
                                </a>
                                                                    <ul>
                                                                                        <li>
                                                    <a href="setup/setupCategory">
                                                        <span class="title">Setup Category</span>
                                                    </a>
                                                </li>
                                                                                                <li>
                                                    <a href="setup/allSetup">
                                                        <span class="title">All Setup</span>
                                                    </a>
                                                </li>
                                                                                    </ul>
                                                            </li>
                                                    <li>
                                <a href="#">
                                    <i class="glyphicon glyphicon-wrench"></i>
                                    <span class="title">Carton Size</span>
                                </a>
                                                                    <ul>
                                                                                        <li>
                                                    <a href="add_new_carton_size">
                                                        <span class="title">Add New Carton Size</span>
                                                    </a>
                                                </li>
                                                                                                <li>
                                                    <a href="carton_size_list">
                                                        <span class="title">Carton Size List</span>
                                                    </a>
                                                </li>
                                                                                    </ul>
                                                            </li>
                                                    <li>
                                <a href="#">
                                    <i class="glyphicon glyphicon-wrench"></i>
                                    <span class="title"> Stock Grade</span>
                                </a>
                                                                    <ul>
                                                                                        <li>
                                                    <a href="setup/setupstockgrade">
                                                        <span class="title">Setup Stock Grade</span>
                                                    </a>
                                                </li>
                                                                                    </ul>
                                                            </li>
                                                    <li>
                                <a href="#">
                                    <i class="glyphicon glyphicon-wrench"></i>
                                    <span class="title">Yarn Count</span>
                                </a>
                                                                    <ul>
                                                                                        <li>
                                                    <a href="add_new_yarn_count">
                                                        <span class="title">Add New Yarn Count</span>
                                                    </a>
                                                </li>
                                                                                                <li>
                                                    <a href="yarn_count_list">
                                                        <span class="title">Yarn count list</span>
                                                    </a>
                                                </li>
                                                                                    </ul>
                                                            </li>
                                            </ul>
                </li> -->
            </ul>  
   
            <ul class="nav navbar-right pull-right">
                <li>
                        <a class="dropdown-toggle" href="{{ route('logout') }}"
                                       onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                       <i class="entypo-list"></i> {{ __('Sign Out') }}
                                    </a>
                                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                        @csrf
                                    </form>
                       
                    </a> 
                </li>
            </ul>
        </div>
    </header>
        <div class="main-content">
                        
<script language="javascript">
    function fadeMessage(msgDivId)
    {
        $(msgDivId).fadeOut('slow');
    }
</script>
            <script>
                    $(function () {
                        $('#popupDatepicker').datepick({
                            changeYear: true,
                            yearRange: "1930:2050",
                            dateFormat: 'yyyy-mm-dd'

                        });
                        $('#date1').datepick({
                            changeYear: true,
                            yearRange: "1930:2050",
                            dateFormat: 'yyyy-mm-dd'

                        });

                        $('#date2').datepick({
                            changeYear: true,
                            yearRange: "1930:2050",
                            dateFormat: 'yyyy-mm-dd'

                        });
                        $('#date3').datepick({
                            changeYear: true,
                            yearRange: "1930:2050",
                            dateFormat: 'yyyy-mm-dd'

                        });
                        $('#date4').datepick({
                            changeYear: true,
                            yearRange: "1930:2050",
                            dateFormat: 'yyyy-mm-dd'

                        });

                        $('#inlineDatepicker').datepick({onSelect: showDate});
                    });

                    function showDate(date) {
                        alert('The date chosen is ' + date);
                    }
            </script>
            <div class="container-fluid">
            <style>
    .head_color{
        color:black!important;
    }
</style>
<ol class="breadcrumb bc-3">
    <li>
        <a href="{{ url('dashboard') }}"><i class="fa-home"></i>Dashboard</a>
    </li>
    <li>
        <a href="{{ route('admin_list') }}">Admin</a>
    </li>
    <li class="active">
        <strong>Add Admin</strong>
    </li>
    <a href="{{ route('admin_list') }}" class="btn btn-red btn-icon pull-right">
        <i class="entypo-cancel"></i>
        Add Cancel
    </a>
</ol>
<div class="col-md-8 col-md-offset-2">
    <div class="panel panel-default" data-collapsed="0">
        <div class="panel-heading"> 
            <div class="panel-title"> Add New Admin</div> 
            <div class="panel-options"> 
                <a href="#sample-modal" data-toggle="modal" data-target="#sample-modal-dialog-1" class="bg">
                    <i class="entypo-cog"></i>
                </a>
                <a href="#" data-rel="collapse"><i class="entypo-down-open"></i></a> 
                <a href="#" data-rel="reload"><i class="entypo-arrows-ccw"></i></a> 
                <a href="#" data-rel="close"><i class="entypo-cancel"></i></a> 
            </div>
        </div>

        <div class="panel-body">
            <form role="form" class="form-horizontal form-groups-bordered" enctype= multipart/form-data method="POST" action="{{ route('save_new_admin') }}">
            	@csrf
                <div class="form-group ">
                    <label for="field-5" class="col-sm-2 control-label head_color">First Name.</label>
                    <div class="col-sm-4">
                        <input type="text" required="" class="form-control" id="field-5" name="first_name" placeholder="Please Type Name">
                    </div>
                    <label for="field-5" class="col-sm-2 control-label head_color">Last Name.</label>
                    <div class="col-sm-4">
                        <input type="text" class="form-control" id="field-5" required="" value="" name="last_name" placeholder="Please Type Last Name">
                    </div>
                </div>
                <div class="form-group ">
                    <label for="field-5" class="col-sm-2 control-label head_color">Phone.</label>
                    <div class="col-sm-4">
                        <input type="text" required="" class="form-control" onkeypress="return isNumber(event)" name="phone" placeholder="Please Type Phone Number">
                    </div>
                    <label for="field-5" class="col-sm-2 control-label head_color">Email.</label>
                    <div class="col-sm-4">
                        <input type="email" required="" class="form-control" id="field-5" value="" name="email" placeholder="Please Type Email Address">
                    </div>
                </div>
                <div class="form-group ">
                    <label for="field-5" class="col-sm-2 control-label head_color">Admin type.</label>
                    <div class="col-sm-4">
                        <select name="admin_type" required="">
                        	<option value="">---Select admin type---</option>
                        	<option value="Master">Master</option>
                        	<option value="User">User</option>
                        </select>                    </div>
                    <label for="field-5" class="col-sm-2 control-label head_color">Admin status</label>
                    <div class="col-sm-4">
                        <select name="admin_status" required="">
                        	<option value="">---Select admin status---</option>
                        	<option value="Active">Active</option>
                        	<option value="Inactive">Inactive</option>
                        </select>
                    </div>
                </div>

                <div class="form-group ">
                    <label for="field-5" class="col-sm-2 control-label head_color">Password.</label>
                    <div class="col-sm-4">
                        <input type="password" required="" class="form-control" id="field-5" name="password" placeholder="Please Type Password">
                    </div>
                </div>
                <div class="form-group ">

                    <script> var fileindex = 1;
                        var inc = 0;
                        var maxImg = 10;
                    </script>
                    
<script type="text/javascript" src="{{ asset('/') }}/scripts/upload_js/ajaxupload.3.5.js"></script>
<link rel="stylesheet" type="text/css" href="{{ asset('/') }}/scripts/upload_js/styles.css">

<script type="text/javascript">
    
    $(function()
    {
        var btnUpload=$('#upload');
        var status=$('#status');
               
        new AjaxUpload(btnUpload, 
        {
            action: '/upload-api/upload-api.php', 
            name: 'uploadfile',
            data : { 'fileindex':fileindex },
            onSubmit: function(file, ext)
            {
                if (! (ext && /^(jpg|png|jpeg|gif|pdf|xls|xlsx|csv|docx)$/.test(ext)))
		            { 
                    alert('Only JPG, PNG, GIF,PDF,XLS,XLSX,CSV,DOC files are allowed');  
                    return false;
                }
                if(inc == maxImg)
                {
                    alert('You have upload maximum upload limit');  
                    return false;
                }
                status.show();
            },
            onComplete: function(file,response)
            {
                status.hide();
                if(response!="error")				
                {
                    generator(response); 
                } 
                else
                {
                    alert('fail...');
                }
            }
        });
    });
    
     function generator(response)
    {
          var str = '';
          if(fileindex == 1)
          {
              str = str+'<span id="delete_avatar_'+inc+'" style="margin-top:30px; "><div class="upload_image1" style="width:100px; margin-right:10px; float:left;"><img class="img-thumbnail" width="90" height="100" src="http://localhost/cyborg/uploads/thumb/'+response+'" border="0" >';
              str = str+'<input type="hidden" id="image_'+inc+'"  name="image[]" value="'+response+'"/></div><div style="color: red;cursor: pointer;float: left;font-size: 21px;font-weight: bold;margin-left: -25px;  margin-top: -11px; padding-top:0;" onclick="delete_avatar('+inc+')">X</div></span>'; 
              $('.image_content_area').append(str);
               //$('.previous_image').hide();
              inc++;
          }
          else if(fileindex == 2)
          {
               //alert('ee');
             // BW will work 
             $('#foto_box').html("<img src='http://localhost/cyborg/uploads/thumb/"+response+"'>");
             $('#logo_input').html( "<input type='hidden' name='logo' id='logo' value='"+response+"'>");            
          }          
    }
</script>





                    <label for="field-5" class="col-sm-2 control-label head_color">Picture</label>
                    <div class="col-sm-4">
                        <div class="img_uBtn">
                            <input type="file" name="image" required="">
                        </div>
                        <div class="" style="clear: both;"></div><br>
                        <div class="image_content_area">
                        </div>
                    </div>
                    <label for="field-5" class="col-sm-2 control-label head_color">Remark</label>
                    <div class="col-sm-4">
                        <textarea class="form-control" required="" id="field-5" name="remark" value="" placeholder="Please Type Remark"></textarea>
                    </div>

                </div>
                <div class="form-group">
                    <div class="col-sm-10"></div>
                    <div class="col-sm-2">
                        <button type="submit" class="btn btn-success btn-icon icon-left pull-right">
                            Save
                            <i class="entypo-check"></i>
                        </button>
                    </div>
                  
                </div>
            </form>
        </div>
    </div>
</div>
<script type="text/javascript">
    $("document").ready(function () {
        $("#UserEmail").keyup(function () {
            var UserEmail = $("#UserEmail").val();
            if (UserEmail != '') {
                $.ajax({
                    url: "/check_uplicate_email",
                    data: {UserEmail: UserEmail},
                    type: "GET",
                    success: function (hr) {
                        if (hr == 1) {
                            document.getElementById('error1').innerHTML = "Opps!! Email Already Use.";

                        } else {
                            document.getElementById('error1').innerHTML = "Opps!! Email Already Use.";
                        }
                    }
                });
            }
        });
    });
</script>


<script>
    function isNumber(evt) {
        evt = (evt) ? evt : window.event;
        var charCode = (evt.which) ? evt.which : evt.keyCode;
        if (charCode > 31 && (charCode < 48 || charCode > 57)) {
            return false;
        }
        return true;
    }
    function delete_avatar(image_id)
    {
        if (confirm("Are you sure to delete?"))
        {
            $('#delete_avatar_' + image_id).remove();
        }
    }



</script>
            </div>
            </div>
</div>

            <script>
                    $(document).ready(function () {
                        //$("input").css("background-color", "#cccccc");
//                        $("input").focus(function () {
//                            $(this).css("background-color", "#cccccc");
//                        });
//                        $("input").blur(function () {
//                            $(this).css("background-color", "#ffffff");
//                        });
//                        $("textarea").focus(function () {
//                            $(this).css("background-color", "#cccccc");
//                        });
//                        $("textarea").blur(function () {
//                            $(this).css("background-color", "#ffffff");
//                        });

                    });
            </script>
            <script type="text/javascript">
                    var responsiveHelper;
                    var breakpointDefinition = {
                        tablet: 1024,
                        phone: 480
                    };
                    var tableContainer;
                    jQuery(document).ready(function ($)
                    {
                        tableContainer = $("#table-1");
                        tableContainer.dataTable({
                            "sPaginationType": "bootstrap",
                            "aLengthMenu": [[10, 25, 50, -1], [10, 25, 50, "All"]],
                            "bStateSave": true,
                            // Responsive Settings
                            bAutoWidth: false,
                            fnPreDrawCallback: function () {
                                // Initialize the responsive datatables helper once.
                                if (!responsiveHelper) {
                                    responsiveHelper = new ResponsiveDatatablesHelper(tableContainer, breakpointDefinition);
                                }
                            },
                            fnRowCallback: function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
                                responsiveHelper.createExpandIcon(nRow);
                            },
                            fnDrawCallback: function (oSettings) {
                                responsiveHelper.respond();
                            }
                        });
                        $(".dataTables_wrapper select").select2({
                            minimumResultsForSearch: -1
                        });
                    }
                    );
            </script>
    
</body>
@endsection('body')
