<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">

	<meta name="viewport" content="width=device-width, initial-scale=1.0" />
	<meta name="description" content="Neon Admin Panel" />
	<meta name="author" content="" />

	<title>Cyborg| Login</title>

	<link rel="stylesheet" href="{{ asset('/') }}/scripts/assets/js/jquery-ui/css/no-theme/jquery-ui-1.10.3.custom.min.css">
	<link rel="stylesheet" href="{{ asset('/') }}/scripts/assets/css/font-icons/entypo/css/entypo.css">
	<link rel="stylesheet" href="{{ asset('/') }}/scripts/http://fonts.googleapis.com/css?family=Noto+Sans:400,700,400italic">
	<link rel="stylesheet" href="{{ asset('/') }}/scripts/assets/css/bootstrap.css">
	<link rel="stylesheet" href="{{ asset('/') }}/scripts/assets/css/neon-core.css">
	<link rel="stylesheet" href="{{ asset('/') }}/scripts/assets/css/neon-theme.css">
	<link rel="stylesheet" href="{{ asset('/') }}/scripts/assets/css/neon-forms.css">
	<link rel="stylesheet" href="{{ asset('/') }}/scripts/assets/css/custom.css">

	<script src="{{ asset('/') }}/scripts/assets/js/jquery-1.11.0.min.js"></script>
	<script>$.noConflict();</script>

	


</head>
<body class="page-body login-page login-form-fall" data-url="http://neon.dev">


<!-- This is needed when you send requests via Ajax -->
<script type="text/javascript">
var baseurl = '';
</script>

<div class="login-container">
	
	<div class="login-header login-caret">
		
		<div class="login-content">
			
			<a  class="logo">
				<img src="{{ asset('/') }}/logo.png" width="120" alt="" />
			</a>
			<!-- progress bar indicator -->
			<div class="login-progressbar-indicator">
				<h3>43%</h3>
				<span>logging in...</span>
			</div>
		</div>
		
	</div>
	
	<div class="login-progressbar">
		<div></div>
	</div>
	
	<div class="login-form">
		
		<div class="login-content">
			
			<div class="form-login-error">
				<h3>Invalid login</h3>
				<p>Enter <strong>demo</strong>/<strong>demo</strong> as login and password.</p>
			</div>
			
           <form method="POST" action="{{ route('login') }}">
                        @csrf
				
				<div class="form-group">
					
					<div class="input-group">
						<div class="input-group-addon">
							<i class="entypo-user"></i>
						</div>
						
						<input type="email" class="form-control" name="email" id="email" placeholder="Username" autocomplete="off" />
					</div>
					
				</div>
				
				<div class="form-group">
					
					<div class="input-group">
						<div class="input-group-addon">
							<i class="entypo-key"></i>
						</div>
						
						<input type="password" class="form-control" name="password" id="password" placeholder="Password" autocomplete="off" />
					</div>
				
				</div>
				
				<div class="form-group">
					<button type="submit" class="btn btn-primary btn-block btn-login">
						<i class="entypo-login"></i>
						Login In
					</button>
				</div>
				
			</form>
			
			
			
		</div>
		
	</div>
	
</div>


	<!-- Bottom scripts (common) -->
	<script src="{{ asset('/') }}/scripts/assets/js/gsap/main-gsap.js"></script>
	<script src="{{ asset('/') }}/scripts/assets/js/jquery-ui/js/jquery-ui-1.10.3.minimal.min.js"></script>
	<script src="{{ asset('/') }}/scripts/assets/js/bootstrap.js"></script>
	<script src="{{ asset('/') }}/scripts/assets/js/joinable.js"></script>
	<script src="{{ asset('/') }}/scripts/assets/js/resizeable.js"></script>
	<script src="{{ asset('/') }}/scripts/assets/js/neon-api.js"></script>
	<script src="{{ asset('/') }}/scripts/assets/js/jquery.validate.min.js"></script>
	<script src="{{ asset('/') }}/scripts/assets/js/neon-login.js"></script>


	<!-- JavaScripts initializations and stuff -->
	<script src="{{ asset('/') }}/scripts/assets/js/neon-custom.js"></script>


	<!-- Demo Settings -->
	<script src="{{ asset('/') }}/scripts/assets/js/neon-demo.js"></script>

</body>
</html>