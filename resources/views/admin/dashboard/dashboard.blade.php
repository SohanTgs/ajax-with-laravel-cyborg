@extends("admin.master.master")

@section('title')
    Admin | | Dashboard 
@endsection('title')

@section('body')
<body class="page-body" cz-shortcut-listen="true">
        <!--<body class="page-body  skin-white loaded">-->
        <div class="page-container  horizontal-menu "><!-- add class "sidebar-collapsed" to close sidebar by default, "chat-visible" to make chat appear always -->
    <header class="navbar navbar-fixed-top  hidden-print"><!-- set fixed position by adding class "navbar-fixed-top" -->
        <div class="navbar-inner">
            <!-- logo -->
            <div style="height:28px !important;" class="navbar-brand">
                <a href="{{ url("/dashboard") }}">
                    <img src="{{ asset('/') }}/black.png" alt="" class="img-thumbnail" width="120">
                </a>
            </div>
            <!-- main menu -->
            <ul class="navbar-nav">
                                    <li>
                        <a href="">
                            <i class="entypo-gauge"></i>
                            <span class="title">Dashboard</span>
                        </a>
                                            </li>
                                    <li>
                        <a href="">
                            <i class="glyphicon glyphicon-user"></i>
                            <span class="title">Admin</span>
                        </a>
                                                    <ul>
                                                                        <li>
                                            <a href="{{ route('admin_list') }}">
                                                <span class="title">Admin List</span>
                                            </a>
                                        </li>
                                                                                <li>
                                            <a href="{{ route('add_new_admin') }}">
                                                <span class="title">Add New Admin</span>
                                            </a>
                                        </li>
                                                                               
                                                                    </ul>
                                            </li>
                                    <li>
                 <!--       <a href="">
                            <i class="glyphicon glyphicon-user"></i>
                            <span class="title">Customer</span>
                        </a>
                                                    <ul>
                                                                        <li>
                                            <a href="add_new_customer">
                                                <span class="title">Add New Customer</span>
                                            </a>
                                        </li>
                                                                                <li>
                                            <a href="customer_list">
                                                <span class="title">Customer List</span>
                                            </a>
                                        </li>
                                                                    </ul>
                                            </li>
                                    <li>
                        <a href="">
                            <i class="glyphicon glyphicon-th-large"></i>
                            <span class="title">Design</span>
                        </a>
                                                    <ul>
                                                                        <li>
                                            <a href="add_new_design">
                                                <span class="title">Add New Design</span>
                                            </a>
                                        </li>
                                                                                <li>
                                            <a href="design_list">
                                                <span class="title">Design List</span>
                                            </a>
                                        </li>
                                                                    </ul>
                                            </li>
                                    <li>
                        <a href="">
                            <i class="glyphicon glyphicon-th-list"></i>
                            <span class="title">Article </span>
                        </a>
                                                    <ul>
                                                                        <li>
                                            <a href="add_new_artical">
                                                <span class="title">Add New Article </span>
                                            </a>
                                        </li>
                                                                                <li>
                                            <a href="artical_list">
                                                <span class="title">Article List</span>
                                            </a>
                                        </li>
                                                                    </ul>
                                            </li>
                                    <li>
                        <a href="">
                            <i class="entypo-gauge"></i>
                            <span class="title">Report</span>
                        </a>
                                                    <ul>
                                                                        <li>
                                            <a href="dispart_plan_form">
                                                <span class="title">Dis-part Plan</span>
                                            </a>
                                        </li>
                                                                                <li>
                                            <a href="accessorize">
                                                <span class="title">Accessorize</span>
                                            </a>
                                        </li>
                                                                    </ul>
                                            </li>
                                <li>
                    <a href="#">
                        <i class="glyphicon glyphicon-cog"></i>
                        <span class="title">Program</span>
                    </a>
                    <ul>
                                                    <li>
                                <a href="#">
                                    <i class="glyphicon glyphicon-book"></i>
                                    <span class="title">Order Program</span>
                                </a>
                                                                    <ul>
                                                                                        <li>
                                                    <a href="add_new_order">
                                                        <span class="title">Add Order Program</span>
                                                    </a>
                                                </li>
                                                                                                <li>
                                                    <a href="order_list">
                                                        <span class="title">Order List</span>
                                                    </a>
                                                </li>
                                                                                    </ul>
                                                            </li>
                                                    <li>
                                <a href="#">
                                    <i class="glyphicon glyphicon-book"></i>
                                    <span class="title">Stock Program</span>
                                </a>
                                                                    <ul>
                                                                                        <li>
                                                    <a href="add_new_stock">
                                                        <span class="title">Add Stock Program</span>
                                                    </a>
                                                </li>
                                                                                                <li>
                                                    <a href="stock_list">
                                                        <span class="title">StockList</span>
                                                    </a>
                                                </li>
                                                                                    </ul>
                                                            </li>
                                                    <li>
                                <a href="#">
                                    <i class="glyphicon glyphicon-book"></i>
                                    <span class="title">LDP Program</span>
                                </a>
                                                                    <ul>
                                                                                        <li>
                                                    <a href="add_new_ldp">
                                                        <span class="title">Add LDP Program</span>
                                                    </a>
                                                </li>
                                                                                                <li>
                                                    <a href="ldp_list">
                                                        <span class="title">LDP List</span>
                                                    </a>
                                                </li>
                                                                                    </ul>
                                                            </li>
                                                    <li>
                                <a href="#">
                                    <i class="glyphicon glyphicon-book"></i>
                                    <span class="title">Sample Program</span>
                                </a>
                                                                    <ul>
                                                                                        <li>
                                                    <a href="add_new_sample">
                                                        <span class="title">Add Sample Program</span>
                                                    </a>
                                                </li>
                                                                                                <li>
                                                    <a href="sample_list">
                                                        <span class="title">Sample List</span>
                                                    </a>
                                                </li>
                                                                                    </ul>
                                                            </li>
                                            </ul>
                </li>

                <li>
                    <a href="#">
                        <i class="glyphicon glyphicon-cog"></i>
                        <span class="title">Setup</span>
                    </a>
                    <ul>
                                                    <li>
                                <a href="#">
                                    <i class="glyphicon glyphicon-wrench"></i>
                                    <span class="title"> Product Category</span>
                                </a>
                                                                    <ul>
                                                                                        <li>
                                                    <a href="product_category_list">
                                                        <span class="title">Category List</span>
                                                    </a>
                                                </li>
                                                                                    </ul>
                                                            </li>
                                                    <li>
                                <a href="#">
                                    <i class="glyphicon glyphicon-wrench"></i>
                                    <span class="title"> Color</span>
                                </a>
                                                                    <ul>
                                                                                        <li>
                                                    <a href="add_new_color">
                                                        <span class="title">Add New Color</span>
                                                    </a>
                                                </li>
                                                                                                <li>
                                                    <a href="color_list">
                                                        <span class="title">Color List</span>
                                                    </a>
                                                </li>
                                                                                    </ul>
                                                            </li>
                                                    <li>
                                <a href="#">
                                    <i class="glyphicon glyphicon-wrench"></i>
                                    <span class="title">Size</span>
                                </a>
                                                                    <ul>
                                                                                        <li>
                                                    <a href="add_new_size">
                                                        <span class="title">Add New Size</span>
                                                    </a>
                                                </li>
                                                                                                <li>
                                                    <a href="size_list">
                                                        <span class="title">Size List</span>
                                                    </a>
                                                </li>
                                                                                    </ul>
                                                            </li>
                                                    <li>
                                <a href="#">
                                    <i class="glyphicon glyphicon-align-center"></i>
                                    <span class="title">Product </span>
                                </a>
                                                                    <ul>
                                                                                        <li>
                                                    <a href="product_list">
                                                        <span class="title">Product List</span>
                                                    </a>
                                                </li>
                                                                                                <li>
                                                    <a href="add_new_product">
                                                        <span class="title">Add New Product</span>
                                                    </a>
                                                </li>
                                                                                    </ul>
                                                            </li>
                                                    <li>
                                <a href="#">
                                    <i class="glyphicon glyphicon-book"></i>
                                    <span class="title">Setup Category</span>
                                </a>
                                                                    <ul>
                                                                                        <li>
                                                    <a href="setup/setupCategory">
                                                        <span class="title">Setup Category</span>
                                                    </a>
                                                </li>
                                                                                                <li>
                                                    <a href="setup/allSetup">
                                                        <span class="title">All Setup</span>
                                                    </a>
                                                </li>
                                                                                    </ul>
                                                            </li>
                                                    <li>
                                <a href="#">
                                    <i class="glyphicon glyphicon-wrench"></i>
                                    <span class="title">Carton Size</span>
                                </a>
                                                                    <ul>
                                                                                        <li>
                                                    <a href="add_new_carton_size">
                                                        <span class="title">Add New Carton Size</span>
                                                    </a>
                                                </li>
                                                                                                <li>
                                                    <a href="carton_size_list">
                                                        <span class="title">Carton Size List</span>
                                                    </a>
                                                </li>
                                                                                    </ul>
                                                            </li>
                                                    <li>
                                <a href="#">
                                    <i class="glyphicon glyphicon-wrench"></i>
                                    <span class="title"> Stock Grade</span>
                                </a>
                                                                    <ul>
                                                                                        <li>
                                                    <a href="setup/setupstockgrade">
                                                        <span class="title">Setup Stock Grade</span>
                                                    </a>
                                                </li>
                                                                                    </ul>
                                                            </li>
                                                    <li>
                                <a href="#">
                                    <i class="glyphicon glyphicon-wrench"></i>
                                    <span class="title">Yarn Count</span>
                                </a>
                                                                    <ul>
                                                                                        <li>
                                                    <a href="add_new_yarn_count">
                                                        <span class="title">Add New Yarn Count</span>
                                                    </a>
                                                </li>
                                                                                                <li>
                                                    <a href="yarn_count_list">
                                                        <span class="title">Yarn count list</span>
                                                    </a>
                                                </li>
                                                                                    </ul>
                                                            </li>
                                            </ul>
                </li> -->
            </ul>  
   
            <ul class="nav navbar-right pull-right">
                <li>
                        <a class="dropdown-toggle" href="{{ route('logout') }}"
                                       onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                       <i class="entypo-list"></i> {{ __('Sign Out') }}
                                    </a>
                                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                        @csrf
                                    </form>
                       
                    </a> 
                </li>
            </ul>
        </div>
    </header>
        <div class="main-content">
                        
<script language="javascript">
    function fadeMessage(msgDivId)
    {
        $(msgDivId).fadeOut('slow');
    }
</script>
            <script>
                    $(function () {
                        $('#popupDatepicker').datepick({
                            changeYear: true,
                            yearRange: "1930:2050",
                            dateFormat: 'yyyy-mm-dd'

                        });
                        $('#date1').datepick({
                            changeYear: true,
                            yearRange: "1930:2050",
                            dateFormat: 'yyyy-mm-dd'

                        });

                        $('#date2').datepick({
                            changeYear: true,
                            yearRange: "1930:2050",
                            dateFormat: 'yyyy-mm-dd'

                        });
                        $('#date3').datepick({
                            changeYear: true,
                            yearRange: "1930:2050",
                            dateFormat: 'yyyy-mm-dd'

                        });
                        $('#date4').datepick({
                            changeYear: true,
                            yearRange: "1930:2050",
                            dateFormat: 'yyyy-mm-dd'

                        });

                        $('#inlineDatepicker').datepick({onSelect: showDate});
                    });

                    function showDate(date) {
                        alert('The date chosen is ' + date);
                    }
            </script>
            <div class="container-fluid">
            <hr>
<div class="row text-center">
    <div class="col-sm-3 col-xs-6 col-md-offset-2">

        <div class="tile-stats tile-red">
            <div class="icon"><i class="entypo-users"></i></div>
            <div class="num" data-start="0" data-end="0" data-postfix="" data-duration="1500" data-delay="0">0</div>
            <h3>Total Customer</h3>
        </div>
    </div>


    <div class="col-sm-3 col-xs-6">
        <div class="tile-stats tile-aqua">
            <div class="icon"><i class="entypo-mail"></i></div>
            <div class="num" data-start="0" data-end="0" data-postfix="" data-duration="1500" data-delay="1200">0</div>
            <h3>Total Design</h3>
        </div>
    </div>
    <div class="col-sm-3 col-xs-6">
        <div class="tile-stats tile-blue">
            <div class="icon"><i class="entypo-rss"></i></div>
            <div class="num" data-start="0" data-end="0" data-postfix="" data-duration="1500" data-delay="1800">0</div>
            <h3>Total Order</h3>
        </div>

    </div>
</div>
            </div>
            </div>
</div>

            <script>
                    $(document).ready(function () {
                        //$("input").css("background-color", "#cccccc");
//                        $("input").focus(function () {
//                            $(this).css("background-color", "#cccccc");
//                        });
//                        $("input").blur(function () {
//                            $(this).css("background-color", "#ffffff");
//                        });
//                        $("textarea").focus(function () {
//                            $(this).css("background-color", "#cccccc");
//                        });
//                        $("textarea").blur(function () {
//                            $(this).css("background-color", "#ffffff");
//                        });

                    });
            </script>
            <script type="text/javascript">
                    var responsiveHelper;
                    var breakpointDefinition = {
                        tablet: 1024,
                        phone: 480
                    };
                    var tableContainer;
                    jQuery(document).ready(function ($)
                    {
                        tableContainer = $("#table-1");
                        tableContainer.dataTable({
                            "sPaginationType": "bootstrap",
                            "aLengthMenu": [[10, 25, 50, -1], [10, 25, 50, "All"]],
                            "bStateSave": true,
                            // Responsive Settings
                            bAutoWidth: false,
                            fnPreDrawCallback: function () {
                                // Initialize the responsive datatables helper once.
                                if (!responsiveHelper) {
                                    responsiveHelper = new ResponsiveDatatablesHelper(tableContainer, breakpointDefinition);
                                }
                            },
                            fnRowCallback: function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
                                responsiveHelper.createExpandIcon(nRow);
                            },
                            fnDrawCallback: function (oSettings) {
                                responsiveHelper.respond();
                            }
                        });
                        $(".dataTables_wrapper select").select2({
                            minimumResultsForSearch: -1
                        });
                    }
                    );
            </script>
    
</body>
@endsection
