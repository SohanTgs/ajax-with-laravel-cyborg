<html lang="en"><head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="Neon Admin Panel">
        <meta name="author" content="">
        <title class=" hidden-print">
            @yield('title')       
        </title>
        <link rel="stylesheet" href="bootstrap/css/bootstrap.css">
        <link rel="stylesheet" href="css/formValidation.css">
        <link rel="icon" href="{{ asset('/') }}/scripts/assets/images/favic.png" type="image/x-icon">
        <link rel="stylesheet" href="{{ asset('/') }}/scripts/assets/js/jquery-ui/css/no-theme/jquery-ui-1.10.3.custom.min.css">
        <link rel="stylesheet" href="{{ asset('/') }}/scripts/assets/css/font-icons/entypo/css/entypo.css">
        <link rel="stylesheet" href="http://fonts.googleapis.com/css?family=Noto+Sans:400,700,400italic">
        <link rel="stylesheet" href="{{ asset('/') }}/scripts/assets/css/bootstrap.css">
        <link rel="stylesheet" href="{{ asset('/') }}/scripts/assets/css/neon-core.css">
        <link rel="stylesheet" href="{{ asset('/') }}/scripts/assets/css/neon-theme.css">
        <link rel="stylesheet" href="{{ asset('/') }}/scripts/assets/css/neon-forms.css">
        <link rel="stylesheet" href="{{ asset('/') }}/scripts/assets/css/custom.css">
        <link rel="stylesheet" href="{{ asset('/') }}/style/styles2.css">
        <!--<link rel="stylesheet" href="{{ asset('/') }}/scripts/assets/      css/skins/white.css">-->
        <link href="{{ asset('/') }}/styles/message.css" rel="stylesheet">
        <!--<script src="scripts/assets/js/jquery-1.11.0.min.js"></script>-->
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
        <script>
         /*   var site_Url = '';

            function checkadd() {
                var chk = confirm("Are you sure to add this record ?");
                if (chk) {
                    return true;
                } else {
                    return false;
                }
            }

*/
        </script>
        <style>


        </style>
    </head>
    
    @yield('body')
            <link rel="stylesheet" href="{{ asset('/') }}/scripts/assets/js/select2/select2-bootstrap.css">
            <link rel="stylesheet" href="{{ asset('/') }}/scripts/assets/js/select2/select2.css">
            <link rel="stylesheet" href="{{ asset('/') }}/scripts/assets/js/selectboxit/jquery.selectBoxIt.css">
            <link rel="stylesheet" href="{{ asset('/') }}/scripts/assets/js/daterangepicker/daterangepicker-bs3.css">
            <link rel="stylesheet" href="{{ asset('/') }}/scripts/assets/js/icheck/skins/minimal/_all.css">
            <link rel="stylesheet" href="{{ asset('/') }}/scripts/assets/js/icheck/skins/square/_all.css">
            <link rel="stylesheet" href="{{ asset('/') }}/scripts/assets/js/icheck/skins/flat/_all.css">
            <link rel="stylesheet" href="{{ asset('/') }}/scripts/assets/js/icheck/skins/futurico/futurico.css">
            <link rel="stylesheet" href="{{ asset('/') }}/scripts/assets/js/icheck/skins/polaris/polaris.css">

            <script src="{{ asset('/') }}/scripts/assets/js/select2/select2.min.js"></script>
            <script src="{{ asset('/') }}/scripts/assets/js/bootstrap-tagsinput.min.js"></script>
            <script src="{{ asset('/') }}/scripts/assets/js/typeahead.min.js"></script>
            <script src="{{ asset('/') }}/scripts/assets/js/selectboxit/jquery.selectBoxIt.min.js"></script>
            <script src="{{ asset('/') }}/scripts/assets/js/bootstrap-datepicker.js"></script>
            <script src="{{ asset('/') }}/scripts/assets/js/bootstrap-timepicker.min.js"></script>
            <script src="{{ asset('/') }}/scripts/assets/js/bootstrap-colorpicker.min.js"></script>
            <script src="{{ asset('/') }}/scripts/assets/js/daterangepicker/moment.min.js"></script>
            <script src="{{ asset('/') }}/scripts/assets/js/daterangepicker/daterangepicker.js"></script>
            <script src="{{ asset('/') }}/scripts/assets/js/jquery.multi-select.js"></script>
            <script src="{{ asset('/') }}/scripts/assets/js/icheck/icheck.min.js"></script>
            <script src="{{ asset('/') }}/scripts/assets/js/neon-chat.js"></script>



            <!--------edit for datatable -------->
            <link rel="stylesheet" href="{{ asset('/') }}/scripts/assets/js/datatables/responsive/css/datatables.responsive.css">
            <link rel="stylesheet" href="{{ asset('/') }}/scripts/assets/js/select2/select2-bootstrap.css">
            <link rel="stylesheet" href="{{ asset('/') }}/scripts/assets/js/select2/select2.css">
            <script src="{{ asset('/') }}/scripts/assets/js/jquery.dataTables.min.js"></script>
            <script src="{{ asset('/') }}/scripts/assets/js/datatables/TableTools.min.js"></script>
            <script src="{{ asset('/') }}/scripts/assets/js/dataTables.bootstrap.js"></script>
            <script src="{{ asset('/') }}/scripts/assets/js/datatables/jquery.dataTables.columnFilter.js"></script>
            <script src="{{ asset('/') }}/scripts/assets/js/datatables/lodash.min.js"></script>
            <script src="{{ asset('/') }}/scripts/assets/js/datatables/responsive/js/datatables.responsive.js"></script>
            <script src="{{ asset('/') }}/scripts/assets/js/select2/select2.min.js"></script>
            <script src="{{ asset('/') }}/scripts/assets/js/neon-chat.js"></script>
            <!--------edit for datatable -------->



            <!-- Imported styles on this page -->
            <!--<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>-->
            <link rel="stylesheet" href="{{ asset('/') }}/scripts/assets/js/jvectormap/jquery-jvectormap-1.2.2.css">
            <link rel="stylesheet" href="{{ asset('/') }}/scripts/assets/js/rickshaw/rickshaw.min.css">
            <!-- Bottom scripts (common) -->
            <script src="{{ asset('/') }}/scripts/assets/js/gsap/main-gsap.js"></script>
            <script src="{{ asset('/') }}/scripts/assets/js/jquery-ui/js/jquery-ui-1.10.3.minimal.min.js"></script>
            <script src="{{ asset('/') }}/scripts/assets/js/bootstrap.js"></script>
            <script src="{{ asset('/') }}/scripts/assets/js/joinable.js"></script>
            <script src="{{ asset('/') }}/scripts/assets/js/resizeable.js"></script>
            <script src="{{ asset('/') }}/scripts/assets/js/neon-api.js"></script>
            <script src="{{ asset('/') }}/scripts/assets/js/jvectormap/jquery-jvectormap-1.2.2.min.js"></script>

            <!-- Imported scripts on this page -->
            <script src="{{ asset('/') }}/scripts/assets/js/jvectormap/jquery-jvectormap-europe-merc-en.js"></script>
            <script src="{{ asset('/') }}/scripts/assets/js/jquery.sparkline.min.js"></script>
            <script src="{{ asset('/') }}/scripts/assets/js/rickshaw/vendor/d3.v3.js"></script>
            <script src="{{ asset('/') }}/scripts/assets/js/rickshaw/rickshaw.min.js"></script>
            <script src="{{ asset('/') }}/scripts/assets/js/raphael-min.js"></script>
            <script src="{{ asset('/') }}/scripts/assets/js/morris.min.js"></script>
            <script src="{{ asset('/') }}/scripts/assets/js/toastr.js"></script>
            <script src="{{ asset('/') }}/scripts/assets/js/neon-chat.js"></script>
            <!-- JavaScripts initializations and stuff -->
            <script src="{{ asset('/') }}/scripts/assets/js/neon-custom.js"></script>
            <!-- Demo Settings -->
            <script src="{{ asset('/') }}/scripts/assets/js/neon-demo.js"></script>
            <link href="{{ asset('/') }}/scripts/datepicker/jquery.datepick.css" rel="stylesheet">
            <script src="{{ asset('/') }}/scripts/datepicker/jquery.plugin.js"></script>
            <script src="{{ asset('/') }}/scripts/datepicker/jquery.datepick.js"></script>
            <script type="text/javascript" src="validation/dist/js/formValidation.js"></script>
            <script type="text/javascript" src="validation/dist/js/framework/bootstrap.js"></script>

</html>