<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/',[
	'uses'=>'Welcome@index',
	'as'=>'index'
]);

Route::get('/admin-list',[
	'uses'=>'Welcome@admin_list',
	'as'=>'admin_list'
]);

Route::get('/add-new-admin',[
	'uses'=>'Welcome@add_new_admin',
	'as'=>'add_new_admin'
]);

Route::POST('/save-new-admin',[
	'uses'=>'Welcome@save_new_admin',
	'as'=>'save_new_admin'
]);

Route::get('/ajaxHome',[
	'uses'=>'Welcome@ajaxHome',
	'as'=>'ajaxHome'
]);

Route::POST('/ajaxPost',[
	'uses'=>'Welcome@ajaxPost',
	'as'=>'ajaxPost'
]);

Route::get('/ajaxDel/{id}',[
	'uses'=>'Welcome@ajaxDel',
	'as'=>'ajaxDel'
]);

Route::get('/getTable',[
	'uses'=>'Welcome@getTable',
	'as'=>'getTable'
]);

Route::get('/editId/{id}',[
	'uses'=>'Welcome@editId',
	'as'=>'editId'
]);

Route::POST('/ajaxUpdate',[
	'uses'=>'Welcome@ajaxUpdate',
	'as'=>'ajaxUpdate'
]);


Auth::routes();

Route::get('/dashboard', 'HomeController@index')->name('home');
