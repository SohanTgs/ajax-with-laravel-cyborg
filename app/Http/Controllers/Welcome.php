<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\Ajax;


class Welcome extends Controller
{
    public function index(){
    	return view("admin.login.login");
    }

     public function admin_list(){
     	$users = User::all();
    	return view('admin.admin_list.admin_list',['users'=>$users]);
    }

    public function add_new_admin(){
    	return view('admin.add_new_admin.add_new_admin');
    }
  
    public function save_new_admin(Request $request){
    //	return $request->all();
    		$picture = $request->file('image');
    		$Name = $picture->getClientOriginalName();
			$directory = 'admin/';
			$ImageUrl = $directory.$Name;
			$picture->move($directory,$Name);

    	$user = new User();
    	$user->first_name = $request->first_name;
    	$user->last_name = $request->last_name;
    	$user->phone = $request->phone;
    	$user->image = $ImageUrl;
    	$user->email = $request->email;
    	$user->password = bcrypt($request->password);
    	$user->remark = $request->remark;
    	$user->admin_type = $request->admin_type;
    	$user->admin_status = $request->admin_status;
    	$user->save();
    	return redirect('/admin-list');
    }

    public function ajaxHome(){     
        return view('ajaxHome');
    }

    public function ajaxPost(Request $request){
       $this->validate($request,[
      'test'=>'required|unique:ajaxes',
    ]);
        $ajax = new Ajax();
        $ajax->test = $request->test;
        $ajax->save();    
        return ['success'=>true,'message'=>'Successfully done the work'];
    }


    public function ajaxDel($id){ 
        $user = Ajax::find($id);
        $user->delete();
       echo 'ok';
    }

    public function getTable(){
        $ajax = Ajax::orderBy('id', 'DESC')->get();
           $i = 1;
        foreach ($ajax as $value) {
            echo  "<tr>";
            echo "<td align='center'>";
            echo $i++;
            echo "</td>";
            echo "<td>";
            echo $value->test;
            echo "</td>";
            echo "<td>";
            echo "<a href='#' onclick='del($value->id)' id='$value->id'>Delete</a>"." || ";
            echo "<button type='button' id='$value->id' onclick='edit($value->id)' data-toggle='modal' class='btn btn-sm btn-success' data-target='#exampleModal'>Update</button>";
            echo "</td>";
            echo "</tr>";
        }
    }

    public function editId($id){
        // return $id;
        $ajax = Ajax::find($id);
        return $ajax;
    }

    public function ajaxUpdate(Request $request){
        $ajax = Ajax::find($request->id2);
        $ajax->test = $request->test;
        $ajax->save();
        return ['success'=>true,'message'=>'Upated Successfully'];
    }


}
